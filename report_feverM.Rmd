---
output: 
  pdf_document: 
    fig_height: 7
papersize: a4
geometry: "left=2.0cm, right=1.0cm, top=2.5cm, bottom=2.0cm"
header-includes:
  - \usepackage{float}
  - \renewcommand{\familydefault}{\sfdefault}
  - \usepackage{booktabs}
  - \usepackage{tabu}
  - \usepackage{longtable}
  - \usepackage{pdflscape}
---
\setlength{\unitlength}{1cm}

\begin{picture}(0,0)(-12.5, -1)
\includegraphics[height = 1.1 cm]{logo/LogoCermel.png}
\end{picture}

\begin{center}
\Large{\textbf{Beyond malaria, the causes of fever in the population of Brazil, Gabon and Malawi}}
\end{center}

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning = FALSE, error = FALSE, message = FALSE)
options(knitr.kable.NA = "-")
library(dplyr)
library(knitr)
library(kableExtra)
```


```{r}
tab_1 %>%
  kable(caption = "Baseline demographics characteristics of participants included in the analysis overall and by site",
        format = "latex", booktabs = TRUE, linesep = linesep(c(1, 5, 2, 6, 3, 3, 3, 5, 1)),
        col.names = c("Variable", " ", "Total", "Brazil", "Gabon", "Malawi", "p-value"),
        align = c("l", "l", "r", "r", "r", "r", "r"),
        format.args = list(big.mark = ' ')) %>%
  kable_styling(latex_options = c("HOLD_position", "scale_down")) %>%
  row_spec(0, bold = TRUE)
```


```{r}
tab_2 %>%
  kable(caption = "Participant Treatment history in the analysis overall and by site",
        format = "latex", booktabs = TRUE,  longtable = F, linesep = linesep(c(3, 3, 3, 4)),
        col.names = c("Variable", " ", "Total", "Brazil", "Gabon", "Malawi"),
        align = c("l", "l", "r", "r", "r", "r"),
        format.args = list(big.mark = ' ')) %>%
  kable_styling(latex_options = c("HOLD_position")) %>%
  row_spec(0, bold = TRUE)
```


```{r}
tab_2_01 %>%
  kable(caption = "Systemic examination in the analysis overall and by site",
        format = "latex", booktabs = TRUE,  longtable = F, linesep = linesep(c(2, 2, 2, 2, 2, 2, 2, 2, 2, 2)),
        col.names = c("Variable", " ", "Total", "Brazil", "Gabon", "Malawi", "p-value"),
        align = c("l", "l", "r", "r", "r", "r", "r"),
        format.args = list(big.mark = ' ')) %>%
  kable_styling(latex_options = c("HOLD_position")) %>%
  row_spec(0, bold = TRUE)
```


```{r}
tab_2_02 %>%
  kable(caption = "Systemic examination specific symtom in the analysis overall and by site",
        format = "latex", booktabs = TRUE,  longtable = F, linesep = "",
        col.names = c("Variable", " ", "Total", "Brazil", "Gabon", "Malawi"),
        align = c("l", "l", "r", "r", "r", "r"),
        format.args = list(big.mark = ' ')) %>%
  kable_styling(latex_options = c("HOLD_position", "scale_down"), font_size = 8) %>%
  row_spec(0, bold = TRUE)
```



```{r}
tab_3 %>%
  kable(caption = "Prevalence of symptoms cited in the analysis overall and by site",
        format = "latex", booktabs = TRUE,  longtable = F, linesep = "",
        col.names = c(" ", "Total", "Brazil", "Gabon", "Malawi"),
        align = c("l", "r", "r", "r", "r"),
        format.args = list(big.mark = ' ')) %>%
  kable_styling(latex_options = c("HOLD_position"), font_size = 9) %>%
  row_spec(0, bold = TRUE)
```


```{r fig.cap="Prevalence of symptoms cited in the analysis overall and by site", fig.pos='h'}
heatmap_1
```


```{r}
tab_4 %>%
  kable(caption = "Pathogens Detected in study samples per sites",
        format = "latex", booktabs = TRUE, linesep = linesep(c(18, 4, 4, 2)),
        col.names = c(" ", "Species", "Total", "Brazil", "Gabon", "Malawi"),
        align = c("l", "l", "r", "r", "r", "r"),
        format.args = list(big.mark = ' ')) %>%
  kable_styling(latex_options = c("HOLD_position")) %>%
  row_spec(0, bold = TRUE)
```


```{r}
tab_5 %>%
  kable(caption = "Pathogens Detected in study samples per age group",
        format = "latex", booktabs = TRUE, linesep = linesep(c(18, 4, 4, 2, 1)),
        col.names = c(" ", "Species", "Total", "Adults", "Children"),
        align = c("l", "l", "r", "r", "r"),
        format.args = list(big.mark = ' ')) %>%
  kable_styling(latex_options = c("HOLD_position")) %>%
  row_spec(c(0, nrow(tab_5)), bold = TRUE)
```


```{r}
tab_6 %>%
  select(-class) %>%
  kable(caption = "Categories of Genus of Bacterial Isolates according to etiology of fever",
        format = "latex", booktabs = TRUE, linesep = "",
        col.names = c(" ", "Total", "Adults", "Children"),
        align = c("l", "r", "r", "r"),
        format.args = list(big.mark = ' ')) %>%
  kable_styling(latex_options = c("HOLD_position")) %>%
  row_spec(c(0, 35), bold = TRUE) %>%
  pack_rows("Isolated bacteria considered causative agent of the fever", 1,  21) %>%
  pack_rows("Isolated bacteria considered non-causative agent of the fever", 22, 23) %>%
  pack_rows(" ", 24, 24)
```


```{r}
tab_7 %>%
  kable(caption = "Pathogens isolated via sample PCR analysis per sites",
        format = "latex", booktabs = TRUE, linesep = "",
        col.names = c(" ", paste0("Total (N = ", tab7_01$Total, ")"),
                      paste0("Brazil (N = ", tab7_01$Brazil, ")"),
                      paste0("Gabon (N = ", tab7_01$Gabon, ")"),
                      paste0("Malawi (N = ", tab7_01$Malawi, ")")),
        align = c("l", "r", "r", "r", "r"),
        format.args = list(big.mark = ' ')) %>%
  kable_styling(latex_options = c("HOLD_position")) %>%
  row_spec(0, bold = TRUE)
```



<!-- ```{r} -->
<!-- tab_4 %>% -->
<!--   kable(caption = "Genus of pathogenic isolated; For Brazil site", -->
<!--         format = "latex", booktabs = TRUE, linesep = linesep(c(10, 2, 2)), -->
<!--         col.names = c(" ", "Species", "Total", "Adults", "Children"), -->
<!--         align = c("l", "l", "r", "r", "r"), -->
<!--         format.args = list(big.mark = ' ')) %>% -->
<!--   kable_styling(latex_options = c("HOLD_position")) %>% -->
<!--   row_spec(c(0, nrow(tab_4)), bold = TRUE) -->
<!-- ``` -->


<!-- ```{r} -->
<!-- tab_5 %>% -->
<!--   kable(caption = "Genus of pathogenic isolates; For Gabon site", -->
<!--         format = "latex", booktabs = TRUE, linesep = linesep(c(3, 3, 1, 2)), -->
<!--         col.names = c(" ", "Species", "Total", "Adults", "Children"), -->
<!--         align = c("l", "l", "r", "r", "r"), -->
<!--         format.args = list(big.mark = ' ')) %>% -->
<!--   kable_styling(latex_options = c("HOLD_position")) %>% -->
<!--   row_spec(c(0, nrow(tab_5)), bold = TRUE) -->
<!-- ``` -->


<!-- ```{r} -->
<!-- tab_6 %>% -->
<!--   kable(caption = "Genus of  pathogenic isolates; For Malawi site", -->
<!--         format = "latex", booktabs = TRUE, linesep = linesep(c(8, 1, 2)), -->
<!--         col.names = c(" ", "Species", "Total", "Adults", "Children"), -->
<!--         align = c("l", "l", "r", "r", "r"), -->
<!--         format.args = list(big.mark = ' ')) %>% -->
<!--   kable_styling(latex_options = c("HOLD_position")) %>% -->
<!--   row_spec(c(0, nrow(tab_6)), bold = TRUE) -->
<!-- ``` -->


<!-- ```{r} -->
<!-- tab_8 %>% -->
<!--   select(-n) %>%  -->
<!--   kable(caption = "Pathogens isolated via sample PCR analysis", -->
<!--         format = "latex", booktabs = TRUE, linesep = linesep(c(14, 14, 18)), -->
<!--         col.names = c(" ", "Species", "Positive (%)"), -->
<!--         align = c("l", "l", "r"), -->
<!--         format.args = list(big.mark = ' ')) %>% -->
<!--   kable_styling(latex_options = c("HOLD_position"), font_size = 9) %>% -->
<!--   row_spec(0, bold = TRUE) %>% -->
<!--   footnote(symbol = c("For chikungunya, n = 294")) -->
<!-- ``` -->


<!-- ```{r fig.cap="Final classification of infection", fig.pos='h'} -->
<!-- fig_2 -->
<!-- ``` -->


<!-- ```{r fig.cap="Final classification of infection", fig.pos='h', fig.height=4, fig.width=7} -->
<!-- fig_3 -->
<!-- ``` -->


```{r fig.height= 10, fig.width=9, fig.cap=" The occurrence of clinical signs and symptoms in patients infected with specific agents.", fig.pos='h'}
fig_4
```

\begin{landscape}

```{r}
tab_reg_01 %>%
  kable(caption = "Association of clinical features with a frequency greater than or equal to 5\\% of all study participants with five possible classifications of AFI etiology",
        format = "latex", booktabs = TRUE, linesep = "",
        col.names = c(" ", "OR", "95%CI", "p-value", "OR", "95%CI", "p-value", "OR", "95%CI", "p-value",
                      "OR", "95%CI", "p-value", "OR", "95%CI", "p-value"),
        align = c("l", "r", "c", "r", "r", "c", "r", "r", "c", "r", "r", "c", "r", "r", "c", "r"),
        format.args = list(big.mark = ' ')) %>%
  kable_styling(latex_options = c("HOLD_position", "scale_down")) %>%
  add_header_above(c(" " = 1, "Cough" = 3, "Headache" = 3, "Abdominal pain" = 3, "Joint pain" = 3,
                     "Sneezing And Rhinorrhoea" = 3), bold = T, italic = T) %>%
  row_spec(0, bold = TRUE)
```

\vspace{-0.75cm}

```{r}
tab_reg_02 %>%
  kable(format = "latex", booktabs = TRUE, linesep = "",
        col.names = c(" ", "OR", "95%CI", "p-value", "OR", "95%CI", "p-value", "OR", "95%CI", "p-value",
                      "OR", "95%CI", "p-value", "OR", "95%CI", "p-value"),
        align = c("l", "r", "c", "r", "r", "c", "r", "r", "c", "r", "r", "c", "r", "r", "c", "r"),
        format.args = list(big.mark = ' ')) %>%
  kable_styling(latex_options = c("HOLD_position", "scale_down")) %>%
  add_header_above(c(" " = 1, "Vomiting" = 3, "Sore Throat" = 3, "Pain while swallowing" = 3, "Diarrhoea" = 3,
                     "Rash" = 3), bold = T, italic = T) %>%
  row_spec(0, bold = TRUE)
```

\vspace{-0.75cm}

```{r}
tab_reg_03 %>%
  kable(format = "latex", booktabs = TRUE, linesep = "",
        col.names = c(" ", "OR", "95%CI", "p-value", "OR", "95%CI", "p-value", "OR", "95%CI", "p-value",
                      "OR", "95%CI", "p-value"),
        align = c("l", "r", "c", "r", "r", "c", "r", "r", "c", "r", "r", "c", "r"),
        format.args = list(big.mark = ' ')) %>%
  kable_styling(latex_options = c("HOLD_position", "scale_down")) %>%
  add_header_above(c(" " = 1, "Redness of the eyes" = 3, "Photophobia" = 3, "Dysuria" = 3, "Chest pain" = 3), bold = T, italic = T) %>%
  row_spec(0, bold = TRUE) %>%
  footnote(general = c("OR is adjusted for age group, gender and study site"))
```

\end{landscape}
